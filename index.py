## Importing Modules

from flask import Flask, render_template, Markup


app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def index():
    return render_template('index.html')



@app.route('/start', methods=['POST', 'GET'])
def start():
    return render_template('start.html')



@app.route('/settings', methods=['POST', 'GET'])
def settings():
    return render_template('settings.html')



"""
Here, this is used as a backend for the start interface view. 

Let's say the user presses a button, this one can be used in 
the backend. 

Followed by variable, output, that is used to show the output
content from the backend to the frontend. 

`output = <something something>`

Followed by the `return statement`, and the `render_template`,
that is used to render and show the output content from the 
backend to the frontend. 

`return render_template('SAME FILE NAME TO RENDER', output=output)`
"""

@app.route('/run_script', methods=['POST'])
def run_script():
    output = print("hello world")
    
    return render_template('start.html', output=output)






if __name__ == '__main__':
    app.run(debug=True, port=4000)
